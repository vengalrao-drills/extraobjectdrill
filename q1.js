// "John": {
//     age: 24,
//     desgination: "Senior Golang Developer",
//     interests: ["Chess, Reading Comics, Playing Video Games"],
//     qualification: "Masters",
//     nationality: "Greenland"
// },

// Q1 Find all users who are interested in playing video games.
const data = require("./data/1-users.cjs");

function q1() {
  let keys = Object.keys(data);  // getting all the keys of the data
  let playGames = [];  
  keys.forEach((index) => {
    data[index]['interests'].forEach((word)=>{  // Looping through each word in the 'interests' array for the current key
        if( (word.toLowerCase()).includes('video')  ){
            playGames.push(index)
        }
    })
  });
  return playGames;  // Returning the array of keys related to video games
}

console.log(q1());
