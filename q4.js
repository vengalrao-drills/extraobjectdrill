// Q4 Find all users with masters Degree.

const data = require("./data/1-users.cjs");

function q4(data) {
  let keys = Object.keys(data); // getting all the keys of the data

  let final = keys.filter((index) => {
    let qualification = data[index]["qualification"].toLowerCase();
    if (qualification.includes("masters")) {
      // changed to lowercase and finding the letter that has 'masters'
      return true;
    }
  });
  return final;
}
console.log(q4(data));

// function checkIt(Array , word ) {
//   if (Array.includes(word)) {
//     return true;
//   }
//   return false;
// }

// function q4(data) {
//   let MastersStudent = [];
//   for (let index in data) {
//     let check = checkIt(data[index]["qualification"] , 'Masters' );
//     if (check != false) {
//         MastersStudent.push(index);
//     }
//   }
//   return MastersStudent;
// }

// console.log(q4(data))
