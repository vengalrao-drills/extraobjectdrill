// "John": {
//     age: 24,
//     desgination: "Senior Golang Developer",
//     interests: ["Chess, Reading Comics, Playing Video Games"],
//     qualification: "Masters",
//     nationality: "Greenland"
// },

// Q2 Find all users staying in Germany.
const data = require("./data/1-users.cjs");

function q2(data) {
  let keys = Object.keys(data);  // getting all the keys of the data

  let final = keys.filter((index) => {
    let nationality = data[index]["nationality"].toLowerCase();
    if (nationality.includes("germany")) {    // Checking if the nationality includes the word 'germany'
      return true;
    }
  });
  return final; // filtered data
}
console.log(q2(data));

// function checkIt(Array , word ) {
//   if (Array.includes(word)) {
//     return true;
//   }
//   return false;
// }

// function q2(data) {
//   let playGames = [];
//   for (let index in data) {
//     let check = checkIt(data[index]["nationality"] , 'Germany' );
//     if (check != false) {
//       playGames.push(index);
//     }
//   }
//   return playGames;
// }

// console.log(q2(data))
