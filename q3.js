// Q3 Sort users based on their seniority level 

// for Designation - Senior Developer > Developer > Intern
// for Age - 20 > 10

// "John": {
//     age: 24,
//     desgination: "Senior Golang Developer",
//     interests: ["Chess, Reading Comics, Playing Video Games"],
//     qualification: "Masters",
//     nationality: "Greenland"
// },

const data = require('./data/1-users.cjs') 
let stagesOfLife = ['intern' ,  'developer' , 'senior' ]  // taken data as per the data  provided
let indexes = [1,2,3]  // indexes given based on stagesOfLife

function checkIndex( a, b ){
    let aValue = -1;  // given sample data for both aValue , bValue
    let bValue = -1;    

    // here in that data. the deignation has both the senior developer , developer, intern.
    // so  i have given 'senior developer' at the end of the array. as it goes loop through out the array. if developer is in the end. it is considered as developer.
    stagesOfLife.forEach((index)=>{
        if((a.toLowerCase()).includes(index)    ){ 
            aValue = stagesOfLife.indexOf(index) 
        }
        if(  (b.toLowerCase()).includes(index) ){
            bValue = stagesOfLife.indexOf(index) 
        }
    }) 
    return [aValue , bValue] // returing the indev values
}

function q3(data){ 
    let dataReformed = Object.entries(data);
    let final = dataReformed.sort((a,b)=>{
        // a and b are the data -looped inside.
        aAge = a[1].age
        bAge = b[1].age 
        aExp = a[1].desgination
        bExp = b[1].desgination
        let valReceived = checkIndex(aExp , bExp); // priotizing according to the values
        if(valReceived[0] != valReceived[1]){
            return valReceived[1]-valReceived[0]
        }else{
            return bAge-aAge
        }
    })
    return final // returning the final array (sorted array)
}

console.log(q3(data))
// console.log(data)