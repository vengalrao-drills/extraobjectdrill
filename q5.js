// "John": {
//     age: 24,
//     desgination: "Senior Golang Developer",
//     interests: ["Chess, Reading Comics, Playing Video Games"],
//     qualification: "Masters",
//     nationality: "Greenland"
// },

// Q5 Group users based on their Programming language mentioned in their designation.
// NOTE: Do not change the name of this file

const data = require("./data/1-users.cjs");
let typesOfLanguages = ["golang", "javascript", "python"];

function checkIt(word, typesOfLanguages) {
  let answer = "";
  typesOfLanguages.forEach((index) => {
    if (word.includes(index)) {
      answer = index;
    }
  });
  
  return answer.toUpperCase();
}

function q5(data, typesOfLanguages) {
  let keys = Object.keys(data);  // getting all the keys of the data

  let final =  {};
  keys.forEach((index) => {
    let designation = data[index]["desgination"].toLowerCase();
    let answer = checkIt(designation, typesOfLanguages);
    if (final[answer]== undefined ){  // checking whether the 'answer'(language) is present n dictionary or not!
      final[answer] = [index]
    }else{
      final[answer].push(index) // if present push the name to the respective language
    }

  });
  return final;
}

console.log(q5(data, typesOfLanguages));

// function checkIt(language){
//   let AllLanguages = ['golang' , 'javascript'  , 'python' ];
//   for(let i=0; i<AllLanguages.length; i++){
//     if((language.toLowerCase()).includes(AllLanguages[i])){
//         return AllLanguages[i].toUpperCase()
//     }
//   }
// }

// function q5(data) {
//   let allLanguages = {};
//   for (let index in data) {
//     let checkLang = checkIt(data[index]['desgination']) ;
//     if ( checkLang !== undefined ){
//       if (allLanguages[checkLang] !== undefined){
//         allLanguages[checkLang].push(index)
//       }else{
//         allLanguages[checkLang] = [index]
//       }
//     }
//   }
//   return allLanguages
// }

// console.log(q5(data));
